module.exports = function(grunt) {

  var connect = require('connect');

  //below is the list of files in client/javascript/ that should be minified
  var minifyJsFiles = ['app.js','css.js', 'css-loader.js', 'main.js',
    'overlay.js', 'vt_intl.js', 'xlsx.js', 'jszip.js', 'jquery.js'
  ];
  minifyJsFiles = minifyJsFiles.map(function(x){return 'client/javascripts/' + x;});
  var nonMinifyJsFiles = ['client/javascripts/**' ].concat(minifyJsFiles.map(function(x){
    return '!' + x;}));

  grunt.initConfig({
    connect: {
      devenv: { // start a web server in dev environment
        options: {
          port: 8000,
          keepalive: true,
          base: {
            path: 'dev/client',
            options: {
              index: 'index.html',
              maxAge: 300000
            }
          }
        }
      },
      distenv: { // start a web server in dist environment
        options: {
          port: 8001,
          keepalive: true,
          base: {
            path: 'dist/client',
            options: {
              index: 'index.html',
              maxAge: 300000
            }
          }
        }
      }
    },

    exec: {
        devenv: { // start the node app in dev environment
            cwd: 'dev/server/',
            cmd:'node app.js'
        },
        distenv: { // start the node app in dist environment
            cwd: 'dist/server',
            cmd: 'node app.js'
        },
        testserver: { // run the test suite on the node app
          cwd: '.',
          cmd: 'node node_modules/mocha/bin/mocha --colors dev/server/tests/unit'
        },
        testclient: { // open the client test suite page in a web browser
          cmd: 'http://localhost:8000/unitTest.html &'
        },
        functionalTest: { // open the client functional test suite page in a web browser
          cmd: 'http://localhost:8000/functionalTest.html &'
        }
    },

    copy: { // copy the files from dev environment to dist environment
            // except for the Javascript files that have to be minified and the client test files
      main: {
        files: [
          {expand: true, cwd: 'dev', src: ['server/**', '!server/node_modules/**'], dest: 'dist/'},
          {expand: true,
           cwd: 'dev',
           src: ['client/**',
                 '!client/javascripts/**',
                 '!client/tests/**',
                 '!client/*.html',
                 '!client/data/**'],
           dest: 'dist/'},
          {expand: true, cwd: 'dev', src: nonMinifyJsFiles, dest: 'dist/'}
        ],
      },
    },

    sync: { // synchronize the dev environment and the dist environment
            // except for the Javascript files that have to be minified and the client test files
      main: {
        files: [
          {cwd: 'dev',
           src: ['server/**', '!server/node_modules/**'],
           dest: 'dist/'},
          {cwd: 'dev',
           src: ['client/**',
                 '!client/javascripts/**',
                 '!client/tests/**',
                 '!client/*.html',
                 '!client/data/**'],
           dest: 'dist/'},
          {cwd: 'dev', src: nonMinifyJsFiles, dest: 'dist/'}
        ],
        verbose: false,
        pretend: false, // if true, don't do any disk operations - just write log
        //ignoreInDest: "**/*.js", // Never remove js files from destination
        updateAndDelete: true // Remove all files from dest that are not found in src

      }
    },

    uglify: { // minify the required JavaScript files
      js: {
        files: [{
          expand: true,
          cwd: 'dev',
          src: minifyJsFiles,
          dest: 'dist/'
        }]

      }
    },

    replace: {
      replacePort: {
        src: ['dev/client/index.html'],             // source files array (supports minimatch)
        dest: 'dist/client/',             // destination directory or file
        replacements: [{
          from: 'localhost:8000',                   // string replacement
          to: 'localhost:8001'
        }]
      },
      removeDevDependencies: {
        src: ['package.json'],
        dest: 'dist/server/',
        replacements: [{
          from: /(,\s*"devDependencies": {[^}]*})/,
          to: ''
        }]
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-text-replace');


  // minify a set of JavaScript files defined at top of the file
  grunt.registerTask('default', 'dist');
  // prepare the dist folder for distribution
  grunt.registerTask('dist', ['sync', 'uglify', 'replace:removeDevDependencies']);

  // ***** Node server
  // Start Developement Node Server
  grunt.registerTask('node-dev', ['exec:devenv']);
  // Start Production Node Server
  grunt.registerTask('node-prod', ['exec:distenv']);
  // run the test suite on the server side, in dev environment
  grunt.registerTask('node-dev-test', ['exec:testserver']);


  // ***** Web Server to test visualisation tool
  // Start a Web Server with the dev version of the visualisation tool
  grunt.registerTask('vs-tool-dev', ['connect:devenv']);
  // Start a Web Server with the production version of the visualisation tool
  grunt.registerTask('vs-tool-prod', ['connect:distenv']);
  // run the test suite on the client side, in a web browser, in dev environment
  // Doesn't seem to work for the moment
  // Open http://localhost:8000/unitTest.html in a webrowser instead
  grunt.registerTask('vs-tool-test', ['exec:testclient', 'connect:devenv']);
  // run the test suite on the client side, in a web browser, in dev environment
  // Doesn't seem to work for the moment
  // Open http://localhost:8000/functionalTest.html in a webrowser instead
  grunt.registerTask('vs-tool-functional-test', ['exec:functionalTest', 'connect:devenv']);
};
