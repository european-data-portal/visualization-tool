Visualisation Tool
============================

JavaScript libraries to visualise (statistical) data in tables and graphical charts.

## Installation
The dist folder contains 2 folders, one for the client and one for the server side application.
In order to deploy the application on a server, you need to copy each  folder to the correct location:
* The client folder needs to be copied to the web server
* The server folder needs to be copied to the node server

### Client Side App
The client side app consists of JS libraries and stylesheets.

To load the application you need to add this snippet to your template file in the end of the body tag:
```html
<script id="vt_initializor" src="javascripts/require.js" data-preprocessor-url='http://localhost:3000/visualization-tool' data-main="javascripts/main" async></script>
```
And the following attributes on the tag which will be use to preview the data:
```html
<a href="#" class="file-preview" data-file="http://portal/data/myfile.xlsx">Preview</a>
```

### Server Side App
Install Node JS:
* [Linux (Package manager)](https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager)
* [Windows (Installer)](https://nodejs.org/download/)

Install the dependencies:  
Go into the server folder and run:

```bash
npm install
```

Run in Development
Node Server
```bash
grunt node-dev
```
Client Web Server
```bash
grunt vs-tool-dev
```

Run the server with forever

```bash
# In Development
forever -o dev/server/debug.log start dev/server/app.js
# In Production
forever -o dist/server/debug.log start dist/server/app.js
```
