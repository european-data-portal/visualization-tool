var assert = require('assert');
var sinon = require('sinon');

process.chdir('dev/server');

var files = require('../../routes/files');

var TMP_FOLDER = 'tmp/';

var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + '/' + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

describe('node suite', function() {
  describe('extractWorkbook', function() {

    it('should return a list of sheet names from local file', function(done) {
      this.timeout(15000); // increase the timeout because default of 2000 ms is not enough for big files


      deleteFolderRecursive(TMP_FOLDER);
      files.extractWorkbook('../client/data/big_xlsx_65536.xlsx','', function (workbook){
        var names = workbook.SheetNames;
        assert.deepEqual(names, [
          'loremmmmm',
          'Sheet2',
          'Sheet3'
        ]);
        done();
      });
    });
    var url = 'http://www.leedsdatamill.org/storage/f/2015-01-06T10%3A42%3A14.734Z/long-term-voids-on-a-ward-by-ward-basis.xls';
    var localCachedFile = files.localFilePath(url);
    it('should return a list of sheet names from distant file previously cached as '+localCachedFile, function(done){

      files.extractWorkbook(localCachedFile, url, function(workbook){
        var names = workbook.SheetNames;
        assert.deepEqual(names, [
          'Chart1',
          'Chart2',
          'Sheet1',
          'Sheet2',
          'Sheet3',
          'Compatibility Report'
        ]);
        done();
      });
    });
  });

  describe('getFileStreamFromUrl', function(){

    it('should cache accessed distant file using getFileStreamFromUrl', function(done) {



      var url = 'http://www.leedsdatamill.org/storage/f/2015-01-06T10%3A42%3A14.734Z/long-term-voids-on-a-ward-by-ward-basis.xls';
      var localCachedFile = files.localFilePath(url);
      // first make sure the file doesn't already exist by deleting it
      try {fs.unlinkSync(localCachedFile);}catch(err){}

      var file = files.getFileStreamFromUrl(url);
      file.once('finish', function() {
        //check that file has been created
        try {
          var stats = fs.statSync(localCachedFile);
          done();

        } catch(err){done('local file not existing');}
      });
    });
  });

  describe('cleanCache', function(){

    it('should not delete recent temp files', function(done) {
      var path = TMP_FOLDER + 'mytestfile.txt';
      // create a tmp file in tmp folder
      var fd = fs.openSync(path, 'w') ;

      // make file modified 21 minutes ago
      var mtime = new Date();
      mtime = new Date(mtime.getTime() - (19 * 60 * 1000));

      fs.futimesSync(fd, mtime, mtime);


      fs.closeSync(fd);

      // call cleanCache function

      files.cleanCache(20 * 60, null, function (){
        //check that file has been deleted
        try {
          var stats = fs.statSync(path);
          done();

        } catch(err){assert.equal(true, false);done('The test file was unexpectedly deleted');}
      });
    });

    it('should delete old temp files', function(done) {
      this.timeout(5000);
      var path = TMP_FOLDER + 'mytestfile.txt';
      // create a tmmp file in tmp folder
      var fd = fs.openSync(path, 'w') ;

      // make file modified 21 minutes ago
      var mtime = new Date();
      mtime = new Date(mtime.getTime() - (21 * 60 * 1000));

      fs.futimesSync(fd, mtime, mtime);

      fs.closeSync(fd);

      // call cleanCache function

      files.cleanCache(20 * 60, null, function() {
        //check that file has been deleted
        try {
          var stats = fs.statSync(path);
          done('The test file was not deleted as expected');

        } catch (err) { done(); }
      });
    });

    it.skip('should be fast enough to run often', function(done) {
      this.timeout(5000);

      // make file modified 21 minutes ago
      var mtime = new Date();
      console.error('time at launch is ' + (new Date()).getTime());
      var j = 0, maxLoop = 1000;
      for (i = 0; i < maxLoop; i++) {
        files.cleanCache(20 * 60, null, function() {
          //check that file has been deleted

          if (++j >= maxLoop) {
            console.error('time at stop is   ' + (new Date()).getTime());

            done();
          }
        });
      }
    });

    it('should keep the total size of temp files under a given limit', function(done) {
      this.timeout(5000);
      files.cleanCache(0, null, function (){
        var limit = 10000; //size limit in bytes
        var path1 = TMP_FOLDER + 'mytestfile1.txt';
        var path2 = TMP_FOLDER + 'mytestfile2.txt';
        // create a tmp file in tmp folder

        var fillUpFile = function(fd, size){
          for (var i = 0; i< size/10; i++) {
            fs.writeSync(fd, '0000000000');
          }
        };
        var fd1 = fs.openSync(path1, 'w');
        fillUpFile(fd1, limit);
        fs.closeSync(fd1);
        var i = 500000;
        while(i--); // wait a bit to make sure the second file is generated at least one millisecond after the first
        var fd2 = fs.openSync(path2, 'w');
        fillUpFile(fd2, limit);
        fs.closeSync(fd2);
        // call cleanCache function

        files.cleanCache(null, 11000, function(){
          //check that file has been deleted
          try {
            var stats = fs.statSync(path2);

          } catch(err){done('The youngest file was incorrectly deleted');}
          try {
            var stats = fs.statSync(path1);
            done('The oldest file was not deleted as expected');

          } catch(err){assert.equal(true, true);done();}
        });
      });
    });

    it('should do nothing if parameters are null', function(done) {
      this.timeout(5000);
      files.cleanCache(0, null, function (){
        var limit = 10000; //size limit in bytes
        var path1 = TMP_FOLDER + 'mytestfile1.txt';
        var path2 = TMP_FOLDER + 'mytestfile2.txt';
        // create a tmp file in tmp folder

        var fd1 = fs.openSync(path1, "w") ;
        fs.closeSync(fd1);
        var i = 500000;
        while(i--); // wait a bit to make sure the second file is generated at least one millisecond after the first
        var fd2 = fs.openSync(path2, "w") ;
        fs.closeSync(fd2);
        // call cleanCache function
        files.cleanCache(null, null, function(){
          //check that file has been deleted
          try {
            var stats = fs.statSync(path2);
            stats = fs.statSync(path1);

          } catch(err){done('One of the files was incorrectly deleted');}
          done();
        });
      });
    });

  });

  describe('J.readFile', function(){
    beforeEach(function(){
        deleteFolderRecursive(TMP_FOLDER);
    });
    it('should be called twice when extracting a workbook that is not cached', function(done) {


      sinon.spy(files.J, 'readFile');
      var call = files.J.readFile;

      var url = 'http://www.leedsdatamill.org/storage/f/2015-01-06T10%3A42%3A14.734Z/long-term-voids-on-a-ward-by-ward-basis.xls';
      files.extractWorkbook(files.localFilePath(url), url, function() {
        assert.equal(call.callCount,2);
        call.restore();
        done();
      });
    });
  });
});
